package io.swagger.api;

import io.swagger.model.Account;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-11-15T13:43:27.180Z")

@Controller
public class AccountApiController implements AccountApi {

    private static final Logger log = LoggerFactory.getLogger(AccountApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public AccountApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> addAccount(@ApiParam(value = "Account object that needs to be added" ,required=true )  @Valid @RequestBody Account body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Account> getAccountByAccountNumber(@ApiParam(value = "The accountNumber that needs to be fetched",required=true) @PathVariable("accountNumber") String accountNumber) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/xml")) {
            try {
                return new ResponseEntity<Account>(objectMapper.readValue("<Account>  <id>123456789</id>  <bankId>aeiou</bankId>  <accountNumber>4567896A</accountNumber>  <balance>3.149</balance>  <userId>123456789</userId>  <transferts>  </transferts>  <transactions>  </transactions>  <isActive>true</isActive></Account>", Account.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/xml", e);
                return new ResponseEntity<Account>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Account>(objectMapper.readValue("{  \"transferts\" : [ {    \"bankIdOrigin\" : 5,    \"transfertDate\" : \"2000-01-23T04:56:07.000+00:00\",    \"accountNumberOrigin\" : \"4567896A\",    \"amount\" : 1.706140124150311,    \"id\" : 5,    \"bankIdReceiver\" : 2,    \"accountNumberReceiver\" : \"4567896A\"  }, {    \"bankIdOrigin\" : 5,    \"transfertDate\" : \"2000-01-23T04:56:07.000+00:00\",    \"accountNumberOrigin\" : \"4567896A\",    \"amount\" : 1.706140124150311,    \"id\" : 5,    \"bankIdReceiver\" : 2,    \"accountNumberReceiver\" : \"4567896A\"  } ],  \"bankId\" : \"00345\",  \"balance\" : 6.027456183070403,  \"id\" : 0,  \"accountNumber\" : \"4567896A\",  \"transactions\" : [ {    \"amount\" : -2767.8437336496772,    \"id\" : 9,    \"accountNumber\" : \"4567896A\",    \"transactionDate\" : \"2000-01-23T04:56:07.000+00:00\"  }, {    \"amount\" : -2767.8437336496772,    \"id\" : 9,    \"accountNumber\" : \"4567896A\",    \"transactionDate\" : \"2000-01-23T04:56:07.000+00:00\"  } ],  \"isActive\" : true,  \"userId\" : 1}", Account.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Account>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Account>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> updateAccount(@ApiParam(value = "ID of account to update",required=true) @PathVariable("id") Long id,@ApiParam(value = "is account opened or closed") @RequestParam(value="isActive", required=false)  Boolean isActive) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

}
