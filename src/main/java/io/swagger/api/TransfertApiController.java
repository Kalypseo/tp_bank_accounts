package io.swagger.api;

import io.swagger.model.Transfert;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-11-15T13:43:27.180Z")

@Controller
public class TransfertApiController implements TransfertApi {

    private static final Logger log = LoggerFactory.getLogger(TransfertApiController.class);

    private final ObjectMapper objectMapper;

    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public TransfertApiController(ObjectMapper objectMapper, HttpServletRequest request) {
        this.objectMapper = objectMapper;
        this.request = request;
    }

    public ResponseEntity<Void> addTransfert(@ApiParam(value = "Trasnfert object that needs to be added" ,required=true )  @Valid @RequestBody Transfert body) {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Transfert> getTransfertsByReceiver(@ApiParam(value = "The bank id of the receiver that needs to be fetched",required=true) @PathVariable("bankIdReceiver") Integer bankIdReceiver,@ApiParam(value = "The bank id of the receiver that needs to be fetched",required=true) @PathVariable("accountNumberReceiver") String accountNumberReceiver) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/xml")) {
            try {
                return new ResponseEntity<Transfert>(objectMapper.readValue("<Transfert>  <id>123456789</id>  <bankIdOrigin>123456789</bankIdOrigin>  <accountNumberOrigin>4567896A</accountNumberOrigin>  <bankIdReceiver>123456789</bankIdReceiver>  <accountNumberReceiver>4567896A</accountNumberReceiver>  <transfertDate>2000-01-23T04:56:07.000Z</transfertDate>  <amount>3.149</amount></Transfert>", Transfert.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/xml", e);
                return new ResponseEntity<Transfert>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Transfert>(objectMapper.readValue("{  \"bankIdOrigin\" : 5,  \"transfertDate\" : \"2000-01-23T04:56:07.000+00:00\",  \"accountNumberOrigin\" : \"4567896A\",  \"amount\" : 1.706140124150311,  \"id\" : 5,  \"bankIdReceiver\" : 2,  \"accountNumberReceiver\" : \"4567896A\"}", Transfert.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Transfert>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Transfert>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Transfert> getTransfertsByaccountNumberOrigin(@ApiParam(value = "The accountNumberOrigin that needs to be fetched",required=true) @PathVariable("accountNumberOrigin") String accountNumberOrigin) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/xml")) {
            try {
                return new ResponseEntity<Transfert>(objectMapper.readValue("<Transfert>  <id>123456789</id>  <bankIdOrigin>123456789</bankIdOrigin>  <accountNumberOrigin>4567896A</accountNumberOrigin>  <bankIdReceiver>123456789</bankIdReceiver>  <accountNumberReceiver>4567896A</accountNumberReceiver>  <transfertDate>2000-01-23T04:56:07.000Z</transfertDate>  <amount>3.149</amount></Transfert>", Transfert.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/xml", e);
                return new ResponseEntity<Transfert>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        if (accept != null && accept.contains("application/json")) {
            try {
                return new ResponseEntity<Transfert>(objectMapper.readValue("{  \"bankIdOrigin\" : 5,  \"transfertDate\" : \"2000-01-23T04:56:07.000+00:00\",  \"accountNumberOrigin\" : \"4567896A\",  \"amount\" : 1.706140124150311,  \"id\" : 5,  \"bankIdReceiver\" : 2,  \"accountNumberReceiver\" : \"4567896A\"}", Transfert.class), HttpStatus.NOT_IMPLEMENTED);
            } catch (IOException e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<Transfert>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<Transfert>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> transfertGetAllGet() {
        String accept = request.getHeader("Accept");
        return new ResponseEntity<Void>(HttpStatus.NOT_IMPLEMENTED);
    }

}
