package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.model.Transaction;
import io.swagger.model.Transfert;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Account
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-11-15T13:43:27.180Z")

public class Account   {
  @JsonProperty("id")
  private Long id = null;

  @JsonProperty("bankId")
  private String bankId = "00345";

  @JsonProperty("accountNumber")
  private String accountNumber = null;

  @JsonProperty("balance")
  private Double balance = null;

  @JsonProperty("userId")
  private Long userId = null;

  @JsonProperty("transferts")
  @Valid
  private List<Transfert> transferts = null;

  @JsonProperty("transactions")
  @Valid
  private List<Transaction> transactions = null;

  @JsonProperty("isActive")
  private Boolean isActive = true;

  public Account id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Account bankId(String bankId) {
    this.bankId = bankId;
    return this;
  }

  /**
   * Get bankId
   * @return bankId
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

@Pattern(regexp="^([0-9]){5,}$") 
  public String getBankId() {
    return bankId;
  }

  public void setBankId(String bankId) {
    this.bankId = bankId;
  }

  public Account accountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
    return this;
  }

  /**
   * Get accountNumber
   * @return accountNumber
  **/
  @ApiModelProperty(example = "4567896A", value = "")

@Pattern(regexp="^([0-9]){7,}([a-z])$") 
  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public Account balance(Double balance) {
    this.balance = balance;
    return this;
  }

  /**
   * Get balance
   * @return balance
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Double getBalance() {
    return balance;
  }

  public void setBalance(Double balance) {
    this.balance = balance;
  }

  public Account userId(Long userId) {
    this.userId = userId;
    return this;
  }

  /**
   * Get userId
   * @return userId
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Account transferts(List<Transfert> transferts) {
    this.transferts = transferts;
    return this;
  }

  public Account addTransfertsItem(Transfert transfertsItem) {
    if (this.transferts == null) {
      this.transferts = new ArrayList<Transfert>();
    }
    this.transferts.add(transfertsItem);
    return this;
  }

  /**
   * Get transferts
   * @return transferts
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Transfert> getTransferts() {
    return transferts;
  }

  public void setTransferts(List<Transfert> transferts) {
    this.transferts = transferts;
  }

  public Account transactions(List<Transaction> transactions) {
    this.transactions = transactions;
    return this;
  }

  public Account addTransactionsItem(Transaction transactionsItem) {
    if (this.transactions == null) {
      this.transactions = new ArrayList<Transaction>();
    }
    this.transactions.add(transactionsItem);
    return this;
  }

  /**
   * Get transactions
   * @return transactions
  **/
  @ApiModelProperty(value = "")

  @Valid

  public List<Transaction> getTransactions() {
    return transactions;
  }

  public void setTransactions(List<Transaction> transactions) {
    this.transactions = transactions;
  }

  public Account isActive(Boolean isActive) {
    this.isActive = isActive;
    return this;
  }

  /**
   * Get isActive
   * @return isActive
  **/
  @ApiModelProperty(value = "")


  public Boolean isIsActive() {
    return isActive;
  }

  public void setIsActive(Boolean isActive) {
    this.isActive = isActive;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Account account = (Account) o;
    return Objects.equals(this.id, account.id) &&
        Objects.equals(this.bankId, account.bankId) &&
        Objects.equals(this.accountNumber, account.accountNumber) &&
        Objects.equals(this.balance, account.balance) &&
        Objects.equals(this.userId, account.userId) &&
        Objects.equals(this.transferts, account.transferts) &&
        Objects.equals(this.transactions, account.transactions) &&
        Objects.equals(this.isActive, account.isActive);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, bankId, accountNumber, balance, userId, transferts, transactions, isActive);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Account {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    bankId: ").append(toIndentedString(bankId)).append("\n");
    sb.append("    accountNumber: ").append(toIndentedString(accountNumber)).append("\n");
    sb.append("    balance: ").append(toIndentedString(balance)).append("\n");
    sb.append("    userId: ").append(toIndentedString(userId)).append("\n");
    sb.append("    transferts: ").append(toIndentedString(transferts)).append("\n");
    sb.append("    transactions: ").append(toIndentedString(transactions)).append("\n");
    sb.append("    isActive: ").append(toIndentedString(isActive)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

