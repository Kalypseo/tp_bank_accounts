package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.threeten.bp.OffsetDateTime;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Transfert
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-11-15T13:43:27.180Z")

public class Transfert   {
  @JsonProperty("id")
  private Long id = null;

  @JsonProperty("bankIdOrigin")
  private Long bankIdOrigin = null;

  @JsonProperty("accountNumberOrigin")
  private String accountNumberOrigin = null;

  @JsonProperty("bankIdReceiver")
  private Long bankIdReceiver = null;

  @JsonProperty("accountNumberReceiver")
  private String accountNumberReceiver = null;

  @JsonProperty("transfertDate")
  private OffsetDateTime transfertDate = null;

  @JsonProperty("amount")
  private Double amount = null;

  public Transfert id(Long id) {
    this.id = id;
    return this;
  }

  /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Transfert bankIdOrigin(Long bankIdOrigin) {
    this.bankIdOrigin = bankIdOrigin;
    return this;
  }

  /**
   * Get bankIdOrigin
   * @return bankIdOrigin
  **/
  @ApiModelProperty(value = "")


  public Long getBankIdOrigin() {
    return bankIdOrigin;
  }

  public void setBankIdOrigin(Long bankIdOrigin) {
    this.bankIdOrigin = bankIdOrigin;
  }

  public Transfert accountNumberOrigin(String accountNumberOrigin) {
    this.accountNumberOrigin = accountNumberOrigin;
    return this;
  }

  /**
   * Get accountNumberOrigin
   * @return accountNumberOrigin
  **/
  @ApiModelProperty(example = "4567896A", value = "")

@Pattern(regexp="^([0-9]){7,}([a-z])$") 
  public String getAccountNumberOrigin() {
    return accountNumberOrigin;
  }

  public void setAccountNumberOrigin(String accountNumberOrigin) {
    this.accountNumberOrigin = accountNumberOrigin;
  }

  public Transfert bankIdReceiver(Long bankIdReceiver) {
    this.bankIdReceiver = bankIdReceiver;
    return this;
  }

  /**
   * Get bankIdReceiver
   * @return bankIdReceiver
  **/
  @ApiModelProperty(value = "")


  public Long getBankIdReceiver() {
    return bankIdReceiver;
  }

  public void setBankIdReceiver(Long bankIdReceiver) {
    this.bankIdReceiver = bankIdReceiver;
  }

  public Transfert accountNumberReceiver(String accountNumberReceiver) {
    this.accountNumberReceiver = accountNumberReceiver;
    return this;
  }

  /**
   * Get accountNumberReceiver
   * @return accountNumberReceiver
  **/
  @ApiModelProperty(example = "4567896A", value = "")

@Pattern(regexp="^([0-9]){7,}([a-z])$") 
  public String getAccountNumberReceiver() {
    return accountNumberReceiver;
  }

  public void setAccountNumberReceiver(String accountNumberReceiver) {
    this.accountNumberReceiver = accountNumberReceiver;
  }

  public Transfert transfertDate(OffsetDateTime transfertDate) {
    this.transfertDate = transfertDate;
    return this;
  }

  /**
   * Get transfertDate
   * @return transfertDate
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public OffsetDateTime getTransfertDate() {
    return transfertDate;
  }

  public void setTransfertDate(OffsetDateTime transfertDate) {
    this.transfertDate = transfertDate;
  }

  public Transfert amount(Double amount) {
    this.amount = amount;
    return this;
  }

  /**
   * Get amount
   * minimum: 1
   * @return amount
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

@DecimalMin("1")
  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Transfert transfert = (Transfert) o;
    return Objects.equals(this.id, transfert.id) &&
        Objects.equals(this.bankIdOrigin, transfert.bankIdOrigin) &&
        Objects.equals(this.accountNumberOrigin, transfert.accountNumberOrigin) &&
        Objects.equals(this.bankIdReceiver, transfert.bankIdReceiver) &&
        Objects.equals(this.accountNumberReceiver, transfert.accountNumberReceiver) &&
        Objects.equals(this.transfertDate, transfert.transfertDate) &&
        Objects.equals(this.amount, transfert.amount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, bankIdOrigin, accountNumberOrigin, bankIdReceiver, accountNumberReceiver, transfertDate, amount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Transfert {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    bankIdOrigin: ").append(toIndentedString(bankIdOrigin)).append("\n");
    sb.append("    accountNumberOrigin: ").append(toIndentedString(accountNumberOrigin)).append("\n");
    sb.append("    bankIdReceiver: ").append(toIndentedString(bankIdReceiver)).append("\n");
    sb.append("    accountNumberReceiver: ").append(toIndentedString(accountNumberReceiver)).append("\n");
    sb.append("    transfertDate: ").append(toIndentedString(transfertDate)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

